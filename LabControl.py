import threading
import datetime as dt
import usb.core
import dash
from dash import dcc, html, dash_table, ctx
from dash.dependencies import Input, Output, State
import dash_bootstrap_components as dbc
import pandas as pd
import base64
import io
import queue
import time
import configparser

from influxdb_interface import influx

from devices import owon_xdm1041 as xdm1041
from devices import hanmatek_hm310p as hm310p
from devices import owon_spe6103 as spe6103

app = dash.Dash(external_stylesheets=[dbc.themes.FLATLY], suppress_callback_exceptions=True, serve_locally = False)

DISABLE_PLOTS=None

dmm = None
ht = None
ow = None
tabs = []

dmm_plot = []
dmm_timer = []
if DISABLE_PLOTS:
  dmm_plot = dbc.Col([
          dbc.Row(dcc.Graph(id='dmm_graph')),
          dbc.Row(dash_table.DataTable(id='dmm_tbl',
                        page_size=10,
                        page_current=0,
                        persistence=True)),
          html.Br(),
          html.Div([dbc.Button("Toggle Update", id='dmm_update', n_clicks=0),],className="d-grid gap-2 col-6 mx-auto",),
        ])
  dmm_timer = dcc.Interval(
                  id='dmm_interval',
                  interval=10*1000, # in milliseconds
                  n_intervals=0
              )

dmm_card = dbc.Card([
        dbc.Row([dmm_plot,
        dbc.Col([
          html.Br(),
          html.Br(),
          dbc.Row([html.H1("Volts", id='dmm_function', style={'textAlign': 'center'}),]),
          html.Br(),
          dbc.Row([html.H1("XX.XXX", id='dmm_primary_value', style={'textAlign': 'center'}),]),
          html.Br(),
          html.Br(),
          html.Br(),
          dbc.Row([html.H2("XX.XXX", id='dmm_secondary_value', style={'textAlign': 'center'}),]),
          html.Br(),
          html.Br(),
          html.Div([
            dbc.Button("Download CSV", id="dmm_csv"),
            dcc.Download(id="download_dmm_csv"),
          ],className="d-grid gap-2 col-6 mx-auto mt-auto",)
        ],class_name='flex-column d-flex gap-2')
      ]), dmm_timer,
      dcc.Interval(
                  id='dmm_value_interval',
                  interval=3*1000, # in milliseconds
                  n_intervals=0
      )
      ])

ht_plot = []
ht_timer = []
if DISABLE_PLOTS:
  ht_plot = dbc.Col([
          dbc.Row(dcc.Graph(id='ht_graph')),
          dbc.Row(dash_table.DataTable(id='ht_tbl',
                        page_size=10,
                        page_current=0,
                        persistence=True)),
          html.Br(),
          html.Div([dbc.Button("Toggle Update", id='ht_update', n_clicks=0),],className="d-grid gap-2 col-6 mx-auto",),
        ])
  ht_timer = dcc.Interval(
                  id='ht_interval',
                  interval=10*1000, # in milliseconds
                  n_intervals=0
      )

ht_card = dbc.Card([
        dbc.Row([ht_plot,
          dbc.Col([
          html.Br(),
          html.Br(),
          dbc.Row([html.H1("XX.XXX", id='ht_V_set', style={'textAlign': 'center'}),]),
          dbc.Row([html.H2("XX.XXX", id='ht_V_read', style={'textAlign': 'center', 'color':'grey'}),]),
          html.Br(),
          dbc.Row([html.H1("XX.XXX", id='ht_A_set', style={'textAlign': 'center'}),]),
          dbc.Row([html.H2("XX.XXX", id='ht_A_read', style={'textAlign': 'center', 'color':'grey'}),]),
          html.Br(),
          html.Div([dbc.Button("ON/OFF", id='ht_output'),],className="d-grid gap-2 col-6 mx-auto",),
          html.Br(),
          dbc.InputGroup([
                    dbc.InputGroupText("Voltage"),
                    dbc.Input(placeholder="Voltage", type="number", min=0, max=30, step=0.01, id='ht_V'),
                    dbc.InputGroupText("V"),
                    dbc.Button("Set", color="primary",id='ht_set_V')
                ],
                className="mb-3",
          ),
          dbc.InputGroup([
                    dbc.InputGroupText("Current"),
                    dbc.Input(placeholder="Current", type="number", min=0, max=10, step=0.001, id='ht_A'),
                    dbc.InputGroupText("A"),
                    dbc.Button("Set", color="primary",id='ht_set_A')
                ],
                className="mb-3",
          ),
          dbc.InputGroup([
                    dbc.InputGroupText("OVP"),
                    dbc.Input(placeholder="Voltage", type="number", min=0, max=30, step=0.01, id='ht_OVP'),
                    dbc.InputGroupText("V"),
                    dbc.Button("Set", color="primary",id='ht_set_OVP')
                ],
                className="mb-3",
          ),
          dbc.InputGroup([
                    dbc.InputGroupText("OCP"),
                    dbc.Input(placeholder="Current", type="number", min=0, max=10, step=0.01, id='ht_OCP'),
                    dbc.InputGroupText("A"),
                    dbc.Button("Set", color="primary",id='ht_set_OCP')
                ],
                className="mb-3",
          ),
          html.Br(),
          html.Div([
              dcc.Upload(id='upload_ht_csv', children=html.Div([dbc.Button("Upload Script")],className="d-grid gap-2 mx-auto mt-auto"),multiple=False),
              dbc.Button('Clear Script', id='ht_clear'),
              dbc.Button("Download CSV", id="ht_csv"),
              dcc.Download(id="download_ht_csv"),
            ],className="d-grid gap-2 col-6 mx-auto mt-auto",)
          ],class_name='flex-column d-flex gap-2')
      ]),ht_timer,
      dcc.Interval(
                  id='ht_value_interval',
                  interval=3*1000, # in milliseconds
                  n_intervals=0
      )])

ow_plot = []
ow_timer = []
if DISABLE_PLOTS:
  ow_plot = dbc.Col([
          dbc.Row(dcc.Graph(id='ow_graph')),
          dbc.Row(dash_table.DataTable(id='ow_tbl',
                        page_size=10,
                        page_current=0,
                        persistence=True)),
          html.Br(),
          html.Div([dbc.Button("Toggle Update", id='ow_update', n_clicks=0),],className="d-grid gap-2 col-6 mx-auto",),
        ])
  ow_timer = dcc.Interval(
                  id='ow_interval',
                  interval=10*1000, # in milliseconds
                  n_intervals=0
      )

ow_card = dbc.Card([
        dbc.Row([
        ow_plot,
          dbc.Col([
          html.Br(),
          html.Br(),
          dbc.Row([html.H1("XX.XXX", id='ow_V_set', style={'textAlign': 'center'}),]),
          dbc.Row([html.H2("XX.XXX", id='ow_V_read', style={'textAlign': 'center', 'color':'grey'}),]),
          html.Br(),
          dbc.Row([html.H1("XX.XXX", id='ow_A_set', style={'textAlign': 'center'}),]),
          dbc.Row([html.H2("XX.XXX", id='ow_A_read', style={'textAlign': 'center', 'color':'grey'}),]),
          html.Br(),
          html.Div([dbc.Button("ON/OFF", id='ow_output'),],className="d-grid gap-2 col-6 mx-auto",),
          html.Br(),
          dbc.InputGroup([
                    dbc.InputGroupText("Voltage"),
                    dbc.Input(placeholder="Voltage", type="number", min=0, max=30, step=0.01, id='ow_V'),
                    dbc.InputGroupText("V"),
                    dbc.Button("Set", color="primary",id='ow_set_V')
                ],
                className="mb-3",
          ),
          dbc.InputGroup([
                    dbc.InputGroupText("Current"),
                    dbc.Input(placeholder="Current", type="number", min=0, max=10, step=0.001, id='ow_A'),
                    dbc.InputGroupText("A"),
                    dbc.Button("Set", color="primary",id='ow_set_A')
                ],
                className="mb-3",
          ),
          dbc.InputGroup([
                    dbc.InputGroupText("OVP"),
                    dbc.Input(placeholder="Voltage", type="number", min=0, max=30, step=0.01, id='ow_OVP'),
                    dbc.InputGroupText("V"),
                    dbc.Button("Set", color="primary",id='ow_set_OVP')
                ],
                className="mb-3",
          ),
          dbc.InputGroup([
                    dbc.InputGroupText("OCP"),
                    dbc.Input(placeholder="Current", type="number", min=0, max=10, step=0.01, id='ow_OCP'),
                    dbc.InputGroupText("A"),
                    dbc.Button("Set", color="primary",id='ow_set_OCP')
                ],
                className="mb-3",
          ),
          html.Br(),
          html.Div([
              dcc.Upload(id='upload_ow_csv', children=html.Div([dbc.Button("Upload Script")],className="d-grid gap-2 mx-auto mt-auto"),multiple=False),
              dbc.Button('Clear Script', id='ow_clear'),
              dbc.Button("Download CSV", id="ow_csv"),
              dcc.Download(id="download_ow_csv"),
            ],className="d-grid gap-2 col-6 mx-auto mt-auto",)
          ],class_name='flex-column d-flex gap-2')
      ]),ow_timer,
      dcc.Interval(
                  id='ow_value_interval',
                  interval=3*1000, # in milliseconds
                  n_intervals=0
      )])

def dmm_worker():
  while True:
    try:
      dmm.read_data()
    except:
      pass

def ht_worker():
  while True:
    try:
      ht.read_data()
    except:
      pass

def ow_worker():
  while True:
    try:
      ow.read_data()
    except:
      pass

htq = queue.Queue()
def ht_script_worker():
  df = pd.DataFrame()
  n = -1
  while True:
    if htq.empty():
      if not df.empty:
        ct = dt.datetime.now()
        curr = df.pop(0)
        ht.set_voltage(curr['Voltage'])
        ht.set_current(curr['Current'])
        ht.set_ovp(curr['OVP'])
        ht.set_ocp(curr['OCP'])
        ht.set_output(curr['Output'])
        df = pd.concat([df,curr],axis=1)
        df.columns = range(0,n)
        td = dt.datetime.now() - ct
        sec = float(curr['Time'])
        if td < dt.timedelta(seconds=sec):
          delay = dt.timedelta(seconds=sec) - td
          time.sleep(delay.seconds)
    else:
      df = htq.get()
      df = df.T
      n = len(df.columns)

owq = queue.Queue()
def ow_script_worker():
  df = pd.DataFrame()
  n = -1
  while True:
    if owq.empty():
      if not df.empty:
        ct = dt.datetime.now()
        curr = df.pop(0)
        ow.set_voltage(curr['Voltage'])
        ow.set_current(curr['Current'])
        ow.set_ovp(curr['OVP'])
        ow.set_ocp(curr['OCP'])
        ow.set_output(curr['Output'])
        df = pd.concat([df,curr],axis=1)
        df.columns = range(0,n)
        td = dt.datetime.now() - ct
        sec = float(curr['Time'])
        if td < dt.timedelta(seconds=sec):
          delay = dt.timedelta(seconds=sec) - td
          time.sleep(delay.seconds)
    else:
      df = owq.get()
      df = df.T
      n = len(df.columns)


# DMM
@app.callback(
    Output(component_id='dmm_interval', component_property='disabled'),
    Input(component_id='dmm_tbl', component_property='page_current'),
    Input(component_id='dmm_update', component_property='n_clicks'))
def disable_update(page,u):
    if (page != 0) or (u%2 == 1):
      return True
    return False

@app.callback(Output('dmm_secondary_value', 'style'),
              Input('dmm_value_interval', 'n_intervals'))
def update_dmm_layout(i):
  return {'textAlign': 'center', 'visibility': dmm.get_layout()}

@app.callback(Output('dmm_function', 'children'),
              Input('dmm_value_interval', 'n_intervals'))
def update_dmm_function(i):
  return dmm.get_function()

@app.callback(Output('dmm_primary_value', 'children'),
              Output('dmm_secondary_value', 'children'),
              Input('dmm_value_interval', 'n_intervals'))
def update_dmm_values(i):
  return dmm.get_values()

@app.callback(Output('dmm_tbl', 'data'),
              Output('dmm_tbl', 'columns'),
              Input('dmm_interval', 'n_intervals'))
def update_dmm_table(i):
  tbl, cols = dmm.get_table()
  return tbl, cols

@app.callback(Output('dmm_graph', 'figure'),
              Input('dmm_interval', 'n_intervals'))
def update_dmm_graph(i):
  return dmm.get_graph()

@app.callback(Output("download_dmm_csv", "data"),
              Input("dmm_csv", "n_clicks"),
              prevent_initial_call=True)
def download_dmm_csv(n_clicks):
  df = dmm.get_data()
  return dcc.send_data_frame(df.to_csv, dt.datetime.now().strftime('%Y/%m/%d %H:%M:%S') + "_XDM1041.csv")

# HANMATEK PSU
@app.callback(
    Output(component_id='ht_interval', component_property='disabled'),
    Input(component_id='ht_tbl', component_property='page_current'),
    Input(component_id='ht_update', component_property='n_clicks'))
def disable_update(page,u):
    if (page != 0) or (u%2 == 1):
      return True
    return False

@app.callback(Output('ht_V_set', 'children'),
              Output('ht_A_set', 'children'),
              Output('ht_V_read', 'children'),
              Output('ht_A_read', 'children'),
              Input('ht_value_interval', 'n_intervals'))
def update_ht_values(i):
  return ht.get_values()

@app.callback(Output('ht_output', 'children'),
              Output('ht_output', 'color'),
              Input('ht_output', 'n_clicks'),
              Input('ht_value_interval', 'n_intervals'))
def update_ht_output(n, i):
  triggered_id = ctx.triggered_id
  if triggered_id == 'ht_output':
    ht.toggle_output()
    time.sleep(1)
    return ht.get_output_state()
  elif triggered_id == 'ht_value_interval':
    return ht.get_output_state()

@app.callback(Output('ht_V', 'value'),
              Input('ht_set_V', 'n_clicks'),
              State('ht_V', 'value'),
              prevent_initial_call=True)
def set_ht_V(n, val):
  if val != None:
    ht.set_voltage(val)
  return val

@app.callback(Output('ht_A', 'value'),
              Input('ht_set_A', 'n_clicks'),
              State('ht_A', 'value'),
              prevent_initial_call=True)
def set_ht_A(n, val):
  if val != None:
    ht.set_current(val)
  return val

@app.callback(Output('ht_OVP', 'value'),
              Input('ht_set_OVP', 'n_clicks'),
              State('ht_OVP', 'value'),
              prevent_initial_call=True)
def set_ht_OVP(n, val):
  if val != None:
    ht.set_ovp(val)
  return val

@app.callback(Output('ht_OCP', 'value'),
              Input('ht_set_OCP', 'n_clicks'),
              State('ht_OCP', 'value'),
              prevent_initial_call=True)
def set_ht_OCP(n, val):
  if val != None:
    ht.set_ocp(val)
  return val

@app.callback(Output('ht_tbl', 'data'),
              Output('ht_tbl', 'columns'),
              Input('ht_interval', 'n_intervals'))
def update_ht_table(i):
  return ht.get_table()

@app.callback(Output('ht_graph', 'figure'),
              Input('ht_interval', 'n_intervals'))
def update_ht_graph(i):
  return ht.get_graph()

@app.callback(Output("download_ht_csv", "data"),
              Input("ht_csv", "n_clicks"),
              prevent_initial_call=True)
def download_ht_csv(n_clicks):
  df = ht.get_data()
  return dcc.send_data_frame(df.to_csv, dt.datetime.now().strftime('%Y/%m/%d %H:%M:%S') + "_HM310P.csv")

@app.callback(Output('upload_ht_csv', 'children'),
              Input('upload_ht_csv', 'contents'),
              Input("ht_clear", "n_clicks"),
              State('upload_ht_csv', 'filename'),
              State('upload_ht_csv', 'last_modified'))
def handle_ht_script(content, n, filename,lm):
  triggered_id = ctx.triggered_id
  if triggered_id == 'upload_ht_csv':
    if content is not None:
      content_type, content_string = content.split(',')

      decoded = base64.b64decode(content_string)
      try:
        if 'csv' in filename:
          # Assume that the user uploaded a CSV file
          df = pd.read_csv(
            io.StringIO(decoded.decode('utf-8')))

          htq.put(df)
          threading.Thread(target=ht_script_worker).start()

          return html.Div([dbc.Button("Loaded "+filename+". Upload New Script",color="success")],className="d-grid gap-2 mx-auto mt-auto")
        else:
          return html.Div([dbc.Button("Error! Upload New Script",color="danger")],className="d-grid gap-2 mx-auto mt-auto")

      except Exception as e:
        return html.Div([dbc.Button("Error! Upload New Script",color="danger")],className="d-grid gap-2 mx-auto mt-auto")

  elif triggered_id == 'ht_clear':
    htq.put(pd.DataFrame())

  return html.Div([dbc.Button("Upload Script")],className="d-grid gap-2 mx-auto mt-auto")

# OWON SPE6103
@app.callback(
    Output(component_id='ow_interval', component_property='disabled'),
    Input(component_id='ow_tbl', component_property='page_current'),
    Input(component_id='ow_update', component_property='n_clicks'))
def disable_update(page,u):
    if (page != 0) or (u%2 == 1):
      return True
    return False

@app.callback(Output('ow_V_set', 'children'),
              Output('ow_A_set', 'children'),
              Output('ow_V_read', 'children'),
              Output('ow_A_read', 'children'),
              Input('ow_value_interval', 'n_intervals'))
def update_ow_values(i):
  return ow.get_values()

@app.callback(Output('ow_output', 'children'),
              Output('ow_output', 'color'),
              Input('ow_output', 'n_clicks'),
              Input('ow_value_interval', 'n_intervals'))
def update_ow_output(n, i):
  triggered_id = ctx.triggered_id
  if triggered_id == 'ow_output':
    ow.toggle_output()
    time.sleep(1)
    return ow.get_output_state()
  elif triggered_id == 'ow_value_interval':
    return ow.get_output_state()

@app.callback(Output('ow_V', 'value'),
              Input('ow_set_V', 'n_clicks'),
              State('ow_V', 'value'),
              prevent_initial_call=True)
def set_ow_V(n, val):
  if val != None:
    ow.set_voltage(val)
  return val

@app.callback(Output('ow_A', 'value'),
              Input('ow_set_A', 'n_clicks'),
              State('ow_A', 'value'),
              prevent_initial_call=True)
def set_ow_A(n, val):
  if val != None:
    ow.set_current(val)
  return val

@app.callback(Output('ow_OVP', 'value'),
              Input('ow_set_OVP', 'n_clicks'),
              State('ow_OVP', 'value'),
              prevent_initial_call=True)
def set_ow_OVP(n, val):
  if val != None:
    ow.set_ovp(val)
  return val

@app.callback(Output('ow_OCP', 'value'),
              Input('ow_set_OCP', 'n_clicks'),
              State('ow_OCP', 'value'),
              prevent_initial_call=True)
def set_ow_OCP(n, val):
  if val != None:
    ow.set_ocp(val)
  return val

@app.callback(Output('ow_tbl', 'data'),
              Output('ow_tbl', 'columns'),
              Input('ow_interval', 'n_intervals'))
def update_ow_table(i):
  return ow.get_table()

@app.callback(Output('ow_graph', 'figure'),
              Input('ow_interval', 'n_intervals'))
def update_ow_graph(i):
  return ow.get_graph()

@app.callback(Output("download_ow_csv", "data"),
              Input("ow_csv", "n_clicks"),
              prevent_initial_call=True)
def download_ow_csv(n_clicks):
  df = ow.get_data()
  return dcc.send_data_frame(df.to_csv, dt.datetime.now().strftime('%Y/%m/%d %H:%M:%S') + "_SPE6103.csv")

@app.callback(Output('upload_ow_csv', 'children'),
              Input('upload_ow_csv', 'contents'),
              Input("ow_clear", "n_clicks"),
              State('upload_ow_csv', 'filename'),
              State('upload_ow_csv', 'last_modified'))
def handle_ow_script(content, n, filename,lm):
  triggered_id = ctx.triggered_id
  if triggered_id == 'upload_ow_csv':
    if content is not None:
      content_type, content_string = content.split(',')

      decoded = base64.b64decode(content_string)
      try:
        if 'csv' in filename:
          # Assume that the user uploaded a CSV file
          df = pd.read_csv(
            io.StringIO(decoded.decode('utf-8')))

          owq.put(df)
          threading.Thread(target=ow_script_worker).start()

          return html.Div([dbc.Button("Loaded "+filename+". Upload New Script",color="success")],className="d-grid gap-2 mx-auto mt-auto")
        else:
          return html.Div([dbc.Button("Error! Upload New Script",color="danger")],className="d-grid gap-2 mx-auto mt-auto")

      except Exception as e:
        return html.Div([dbc.Button("Error! Upload New Script",color="danger")],className="d-grid gap-2 mx-auto mt-auto")

  elif triggered_id == 'ow_clear':
    owq.put(pd.DataFrame())

  return html.Div([dbc.Button("Upload Script")],className="d-grid gap-2 mx-auto mt-auto")

def init():
  global dmm
  global ht
  global ow
  global tabs

  ndev = 0
  for i in usb.core.find(idVendor=xdm1041.XDM1041.idVendor, idProduct=xdm1041.XDM1041.idProduct, find_all=True):
    ndev += 1
  dmm_dev = usb.core.find(idVendor=xdm1041.XDM1041.idVendor, idProduct=xdm1041.XDM1041.idProduct, bcdDevice=xdm1041.XDM1041.bcdDevice)
  if dmm_dev is None:
    print('XDM1041 not found')
  else:
    port = '/dev/ttyUSB' + '%d' % (dmm_dev.address-ndev)
    if dmm == None:
      dmm = xdm1041.XDM1041(dmm_lock, influx, port)
      if dmm.ident():
        print('XDM1041: ', port)
        tabs.append(dbc.Tab([dmm_card], label="OWON XDM1041"))
        threading.Thread(target=dmm_worker, daemon=True).start()
      else:
        print('XDM1041: ', port, " Bad Ident")
        try:
          dmm.close()
        except:
          pass
        dmm = None
    else:
      if not dmm.ident():
        print('XDM1041: ', port, " Removed")
        for i, t in enumerate(tabs):
          if str(t) == str(dbc.Tab([dmm_card], label="OWON XDM1041")):
            del (tabs[i])
        try:
          dmm.close()
        except:
          pass
        dmm = None

  ht_dev = usb.core.find(idVendor=hm310p.HM310P.idVendor, idProduct=hm310p.HM310P.idProduct, bcdDevice=hm310p.HM310P.bcdDevice)
  if ht_dev is None:
    print('HT310P not found')
  else:
    port = '/dev/ttyUSB' + '%d' % (ht_dev.address-ndev)
    if ht == None:
      ht = hm310p.HM310P(influx, port)
      if ht.ident():
        print('HT310P: ', port)
        tabs.append(dbc.Tab([ht_card], label="HANMATEK HM310P"))
        threading.Thread(target=ht_worker, daemon=True).start()
      else:
        print('HT310P: ', port, " Bad Ident")
        try:
          ht.close()
        except:
          pass
        ht = None
    else:
      if not ht.ident():
        print('HT310P: ', port, " Removed")
        for i, t in enumerate(tabs):
          if str(t) == str(dbc.Tab([ht_card], label="HANMATEK HM310P")):
            del (tabs[i])
        try:
          ht.close()
        except:
          pass
        ht = None

  ow_dev = usb.core.find(idVendor=spe6103.SPE6103.idVendor, idProduct=spe6103.SPE6103.idProduct, bcdDevice=spe6103.SPE6103.bcdDevice)
  if ow_dev is None:
    print('SPE6103 not found')
  else:
    port = '/dev/ttyUSB' + '%d' % (ow_dev.address-ndev)
    if ow == None:
      ow = spe6103.SPE6103(ow_lock, influx, port)
      if ow.ident():
        print('SPE6103: ', port)
        tabs.append(dbc.Tab([ow_card], label="OWON SPE6103"))
        threading.Thread(target=ow_worker, daemon=True).start()
      else:
        print('SPE6103: ', port, " Bad Ident")
        try:
          ow.close()
        except:
          pass
        ow = None
    else:
      if not ow.ident():
        print('SPE6103: ', port, " Removed")
        for i, t in enumerate(tabs):
          if str(t) == str(dbc.Tab([ow_card], label="OWON SPE6103")):
            del (tabs[i])
        try:
          ow.close()
        except:
          pass
        ow = None

  if len(tabs) == 0:
    return dbc.Alert("No Devices Turned On, Connected or Configured!")
  else:
    return html.Div([dbc.Container([dbc.Tabs(tabs)], fluid=True)])

if __name__ == "__main__":
  dmm_lock = threading.Lock()
  ow_lock = threading.Lock()

  config = configparser.ConfigParser()
  config.read('config.ini')
  influx_url = config['INFLUX']['url']
  influx_token = config['INFLUX']['token']
  influx_org = config['INFLUX']['org']
  DISABLE_PLOTS = bool(config['GENERAL']['plots'])

  influx=influx(influx_url, influx_token, influx_org)

  init()
  app.layout = init

  # Start Dash server
  # threading.Thread(target=run_server).start()
  app.run_server(debug=False, host='0.0.0.0', dev_tools_silence_routes_logging=True)


  htq.join()
  owq.join()
