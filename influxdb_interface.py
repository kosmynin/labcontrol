from datetime import datetime
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client.client.query_api import *
import logging

class influx:

  def __init__(self, url, token, org):
    self.client = InfluxDBClient(url=url, token=token, org=org)
    self.write_api = self.client.write_api(write_options=SYNCHRONOUS)
    self.query_api = self.client.query_api()

  def write(self, measurement, field, val, timestamp=None, tags=None):
    if not timestamp:
      timestamp = datetime.utcnow()
    logging.debug('writing point to influxdb: measurement=%s field=%s val=%s'%(str(measurement),str(field),str(val)))
    p = Point(measurement).field(field, val).time(timestamp, WritePrecision.MS)
    if tags is not None:
      for t in tags:
        p.tag(t[0], t[1])
    logging.debug('point made')
    try:
      self.write_api.write("lab", record=p)
    except Exception as exc:
      logging.error(exc)
      pass
    logging.debug('point written, writer done')

  def get_last(self, measurement, field):
    query = 'from(bucket:"lab")\
    |> range(start: -5m)\
    |> last()\
    |> filter(fn:(r) => r._measurement == "' + measurement + '")\
    |> filter(fn:(r) => r._field == "' + field + '")'
    result = self.query_api.query(query=query)
    results = []
    for table in result:
      for record in table.records:
        results.append(record.get_value())

    return results[0]

  def get_last_minutes(self, measurement, field):
    query = 'from(bucket:"lab")\
    |> range(start: -5m)\
    |> filter(fn:(r) => r._measurement == "' + measurement + '")\
    |> filter(fn:(r) => r._field == "' + field + '")'
    result = self.query_api.query(query=query)
    results = []
    for table in result:
      for record in table.records:
        results.append((record.get_time(),record.get_value()))

    return results
