import devices.com as com
import pandas as pd
import datetime as dt
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from functools import reduce

class SPE6103:
  idVendor=0x1a86
  idProduct=0x7523
  bcdDevice=0x8133

  def __init__(self, lock, influx, port='/dev/ttyUSB0'):
    self.inst = com.SCPI(lock, port)
    self.influx = influx
    if self.ident():
      self.read_data()

  def close(self):
    self.inst.close()

  def read_output(self):
    out = self.query('OUTP?')
    return False if out == 'OFF' else True

  def set_output(self, out):
    self.query('OUTP ' + ('ON' if out else 'OFF'))

  def toggle_output(self):
    self.set_output(not self.read_output())

  def read_current(self):
    amp = float(self.query('MEAS:CURR?'))
    return amp

  def read_power(self):
    watt = float(self.query('MEAS:POW?'))
    return watt

  def read_set_voltage(self):
    volt = float(self.query('VOLT?'))
    return float(volt)

  def set_voltage(self,volt):
    self.query('VOLT ' + volt)

  def read_voltage(self):
    volt = float(self.query('MEAS:VOLT?'))
    return volt

  def read_set_current(self):
    amp = float(self.query('CURR?'))
    return float(amp)

  def set_current(self,amp):
    self.query('CURR ' + amp)

  def read_set_ovp(self):
    volt = float(self.query('VOLT:LIM?'))
    return float(volt)

  def set_ovp(self,volt):
    self.query('VOLT:LIM ' + volt)

  def read_set_ocp(self):
    amp = float(self.query('CURR:LIM?'))
    return float(amp)

  def set_ocp(self,amp):
    self.query('CURR:LIM ' + amp)

  def ident(self):
    try:
      id = self.query('*IDN?')
      if id == '?':
        return False
    except:
      return False
    return True

  def query(self, cmd):
    ret = self.inst.query(cmd)

    return ret

  def read_data(self):
    timestamp = dt.datetime.utcnow()
    sU = self.read_set_voltage()
    sI = self.read_set_current()
    U = self.read_voltage()
    I = self.read_current()
    P = self.read_power()
    OVP = self.read_set_ovp()
    OCP = self.read_set_ocp()
    out = self.read_output()

    self.influx.write("SPE6103","Set Voltage", sU, timestamp)
    self.influx.write("SPE6103","Set Current", sI, timestamp)
    self.influx.write("SPE6103","Set OVP", OVP, timestamp)
    self.influx.write("SPE6103","Set OCP", OCP, timestamp)
    self.influx.write("SPE6103","Voltage", U, timestamp)
    self.influx.write("SPE6103","Current", I, timestamp)
    self.influx.write("SPE6103","Power", P, timestamp)
    self.influx.write("SPE6103","Output", out, timestamp)

  def get_data(self):
    df_sU = pd.DataFrame(self.influx.get_last_minutes('SPE6103','Set Voltage'), columns=['Time', 'Set Voltage'])
    df_sI = pd.DataFrame(self.influx.get_last_minutes('SPE6103','Set Current'), columns=['Time', 'Set Current'])
    df_OVP = pd.DataFrame(self.influx.get_last_minutes('SPE6103','Set OVP'), columns=['Time', 'Set OVP'])
    df_OCP = pd.DataFrame(self.influx.get_last_minutes('SPE6103','Set OCP'), columns=['Time', 'Set OCP'])
    df_P = pd.DataFrame(self.influx.get_last_minutes('SPE6103','Power'), columns=['Time', 'Power'])
    df_U = pd.DataFrame(self.influx.get_last_minutes('SPE6103','Voltage'), columns=['Time', 'Voltage'])
    df_I = pd.DataFrame(self.influx.get_last_minutes('SPE6103','Current'), columns=['Time', 'Current'])
    df_out = pd.DataFrame(self.influx.get_last_minutes('SPE6103','Output'), columns=['Time', 'Output'])
    df = reduce(lambda left,right: pd.merge(left,right, on='Time', how='inner'), [df_sU, df_sI, df_OVP, df_OCP, df_P, df_U, df_I, df_out])

    df['Time'] = df['Time'].apply(lambda x: x.tz_convert('Europe/Berlin'))
    return df

  def get_graph(self):
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    df_U = pd.DataFrame(self.influx.get_last_minutes("SPE6103", 'Voltage'), columns=['Time', 'Voltage'])
    fig.add_trace(go.Scatter(y=df_U['Voltage'], x=df_U['Time'], name='Voltage'), secondary_y=True)
    fig.update_yaxes(title_text='Voltage', secondary_y=True)

    df_I = pd.DataFrame(self.influx.get_last_minutes('SPE6103','Current'), columns=['Time', 'Current'])
    fig.add_trace(go.Scatter(y=df_I['Current'], x=df_I['Time'], name='Current'), secondary_y=False)
    fig.update_yaxes(title_text='Current', secondary_y=False)

    fig.update_xaxes(title_text='Time')
    fig.update_layout(clickmode='event+select')
    return fig

  def get_table(self):
    df = self.get_data()[::-1]
    return df.to_dict(orient='records'), [{"name": i, "id": i} for i in df.columns]

  def get_values(self):
    return "%.2f" % self.influx.get_last('SPE6103','Set Voltage'), "%.3f" % self.influx.get_last('SPE6103','Set Current'), "%.2f" % self.influx.get_last('SPE6103','Voltage'), "%.3f" % self.influx.get_last('SPE6103','Current')

  def get_output_state(self):
    out = self.influx.get_last('SPE6103','Output')
    state = (('ON','success') if out else ('OFF', 'danger'))
    return state
