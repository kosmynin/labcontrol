import devices.com as com
import pandas as pd
import datetime as dt
import plotly.graph_objects as go
from plotly.subplots import make_subplots

class XDM1041:
  idVendor=0x1a86
  idProduct=0x7523
  bcdDevice=0x8134

  modes = {"VOLT":0,
           "CURR":1,
           "FREQ":2,
           "PER":3,
           "CAP":4,
           "CONT":5,
           "DIOD":6,
           "RES":7,
           "TEMP":8,
           "NONe":-1}
  names =  {0:"Voltage",
           1:"Current",
           2:"Frequency",
           3:"Period",
           4:"Capacitance",
           5:"Diode",
           6:"Continuity",
           7:"Resistance",
           8:"Temperature"}

  def __init__(self, lock, influx, port='/dev/ttyUSB0'):
    self.inst = com.SCPI(lock, port)
    self.influx = influx
    if self.ident():
      self.read_data()

  def close(self):
    self.inst.close()

  def ident(self):
    try:
      id = self.query('*IDN?')
      if id == '?':
        return False
    except:
      return False
    return True

  def query(self, cmd):
    CORRECT = False
    while not CORRECT:
      if cmd == 'MODES':
        mode1 = self.query('FUNC1?')
        mode2 = self.query('FUNC2?')
        if (mode1.split(' ')[0] in list(self.modes.keys())[2:-1]) and (mode2.find('NONe') != -1):
          return mode1+';'+mode2
        elif (mode1.split(' ')[0] in list(self.modes.keys())[:2]):
          if (mode1.endswith('AC')) and (mode2.find('NONe') != -1):
            return mode1.split(' ')[0]+';'+mode2
          elif (mode1.endswith('AC')) and (mode2.find('FREQ') != -1):
            return mode1.split(' ')[0]+';'+mode2
          elif not (mode1.endswith('AC')):
            return mode1+';NONe'
      else:
        ret = self.inst.query(cmd)
        if cmd.find('FUNC1') != -1:
          if ret.split(' ')[0] in list(self.modes.keys())[:-1]:
            CORRECT = True
        elif cmd.find('FUNC2') != -1:
          if ret in ['FREQ', 'NONe']:
            CORRECT = True
        elif cmd.find('MEAS') != -1:
          if ret.split(' ')[0] in list(self.modes.keys()):
            CORRECT = False
          else:
            CORRECT = True

          if cmd.find('SHOW') != -1:
            if (not ret[-1].isdigit() and ret.replace('-','')[0].isdigit()) or (ret.upper().find('OVERLOAD') != -1) or (ret.upper().find('OPEN') != -1) :
              CORRECT = True
            else:
              CORRECT = False
          else:
            if ret.replace('E+','').replace('E-','').replace('-','').replace('.','').isdigit():
              CORRECT=True
            else:
              CORRECT=False
        else:
          CORRECT = True

    return ret

  def get_mode(self):
    modes = self.query("MODES").split(';')
    return self.modes[modes[0]], self.modes[modes[1]]

  def get_layout(self):
    _, mode2 = self.get_mode()
    if mode2 != -1:
      return "visible"
    else:
      return "hidden"

  def read_data(self):
    mode1, mode2 = self.get_mode()
    if mode1 < 2:
      if mode2 != -1:
        data2 = self.query("MEAS2:SHOW?")
        data2R = float(self.query("MEAS2?"))
      else:
        data2 = '0.0000Hz'
        data2R = 0.0
      data1 = self.query("MEAS1:SHOW?")
      data1R = float(self.query("MEAS1?"))
      self.influx.write("XDM1041",self.names[mode1], data1)
      self.influx.write("XDM1041",self.names[mode1]+ '_raw', data1R)
      self.influx.write("XDM1041",self.names[2], data2)
      self.influx.write("XDM1041",self.names[2]+ '_raw', data2R)
    else:
      data1 = self.query("MEAS1:SHOW?")
      data1R = float(self.query("MEAS1?"))
      self.influx.write("XDM1041",self.names[mode1], data1)
      self.influx.write("XDM1041",self.names[mode1]+ '_raw', data1R)

  def get_data(self):
    mode1, mode2 =  self.get_mode()
    if mode2 != -1:
      df1 = pd.DataFrame(self.influx.get_last_minutes('XDM1041',self.names[mode1]), columns=['Time', self.names[mode1]])
      df2 = pd.DataFrame(self.influx.get_last_minutes('XDM1041',self.names[mode2]), columns=['Time', self.names[mode2]])
      df = pd.merge(df1, df2)
    else:
      df = pd.DataFrame(self.influx.get_last_minutes('XDM1041',self.names[mode1]), columns=['Time', self.names[mode1]])

    df['Time'] = df['Time'].apply(lambda x: x.tz_convert('Europe/Berlin'))
    return df

  def get_raw_data(self):
    mode1, mode2 =  self.get_mode()
    if mode2 != -1:
      df1 = pd.DataFrame(self.influx.get_last_minutes('XDM1041',self.names[mode1] + '_raw'), columns=['Time', self.names[mode1]])
      df2 = pd.DataFrame(self.influx.get_last_minutes('XDM1041',self.names[mode2] + '_raw'), columns=['Time', self.names[mode2]])
      df = pd.merge(df1, df2)
    else:
      df = pd.DataFrame(self.influx.get_last_minutes('XDM1041',self.names[mode1] + '_raw'), columns=['Time', self.names[mode1]])

    df['Time'] = df['Time'].apply(lambda x: x.tz_convert('Europe/Berlin'))
    return df

  def get_graph(self):
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    df = self.get_raw_data()
    for i, c in enumerate(df.columns[1:]):
      fig.add_trace(go.Scatter(y=df[c], x=df['Time'], name=c), secondary_y=(True if i==0 else False))
      fig.update_yaxes(title_text=c, secondary_y=(True if i==0 else False))
    fig.update_xaxes(title_text='Time')
    return fig

  def get_table(self):
    df = self.get_data()[::-1]
    return df.to_dict(orient='records'), [{"name": i, "id": i} for i in df.columns]

  def get_values(self):
    mode1,mode2 = self.get_mode()
    if mode2 == -1:
      return self.influx.get_last('XDM1041',self.names[mode1]), 0

    return self.influx.get_last('XDM1041',self.names[mode1]),self.influx.get_last('XDM1041',self.names[mode2])

  def get_function(self):
    mode1, _ = self.get_mode()
    return self.names[mode1]
