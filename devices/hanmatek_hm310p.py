import devices.com as com
import pandas as pd
import datetime as dt
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from functools import reduce

class HM310P:
  idVendor=0x1a86
  idProduct=0x7523
  bcdDevice=0x263

  def __init__(self, influx, port='/dev/ttyUSB0'):
    self.inst = com.modbus(port)
    self.influx = influx
    if self.ident():
      self.read_data()

  def close(self):
    self.inst.close()

  def ident(self):
    try:
      self.inst.read(0x03)
    except:
      return False
    return True

  def read_output(self):
    out = bool(self.inst.read(0x01))
    return out

  def set_output(self, out):
    self.inst.write(0x01,int(out))

  def toggle_output(self):
    self.set_output(not self.read_output())

  def read_current(self):
    amp = float(self.inst.read(0x11)/1000)
    return amp

  def read_power(self):
    high = self.inst.read(0x12)
    low = self.inst.read(0x13)
    watt = (high << 4) & (low)
    return float(watt/1000)

  def read_set_voltage(self):
    volt = self.inst.read(0x30)/100
    return float(volt)

  def set_voltage(self,volt):
    volt = int(float(volt)*100)
    self.inst.write(0x30,volt)

  def read_voltage(self):
    volt = float(self.inst.read(0x10)/100)
    return volt

  def read_set_current(self):
    amp = self.inst.read(0x31)/1000
    return float(amp)

  def set_current(self,amp):
    amp = int(float(amp)*1000)
    self.inst.write(0x31,amp)

  def read_set_ovp(self):
    volt = self.inst.read(0x20)/100
    return float(volt)

  def set_ovp(self,volt):
    volt = int(float(volt)*100)
    self.inst.write(0x20,volt)

  def read_set_ocp(self):
    amp = self.inst.read(0x21)/100
    return float(amp)

  def set_ocp(self,amp):
    amp = int(float(amp)*100)
    self.inst.write(0x21,amp)


  def read_data(self):
    timestamp = dt.datetime.utcnow()
    sU = self.read_set_voltage()
    sI = self.read_set_current()
    U = self.read_voltage()
    I = self.read_current()
    P = self.read_power()
    OVP = self.read_set_ovp()
    OCP = self.read_set_ocp()
    out = self.read_output()

    self.influx.write("HM310P","Set Voltage", sU, timestamp)
    self.influx.write("HM310P","Set Current", sI, timestamp)
    self.influx.write("HM310P","Set OVP", OVP, timestamp)
    self.influx.write("HM310P","Set OCP", OCP, timestamp)
    self.influx.write("HM310P","Power", P, timestamp)
    self.influx.write("HM310P","Voltage", U, timestamp)
    self.influx.write("HM310P","Current", I, timestamp)
    self.influx.write("HM310P","Output", out, timestamp)


  def get_data(self):
    df_sU = pd.DataFrame(self.influx.get_last_minutes('HM310P','Set Voltage'), columns=['Time', 'Set Voltage'])
    df_sI = pd.DataFrame(self.influx.get_last_minutes('HM310P','Set Current'), columns=['Time', 'Set Current'])
    df_OVP = pd.DataFrame(self.influx.get_last_minutes('HM310P','Set OVP'), columns=['Time', 'Set OVP'])
    df_OCP = pd.DataFrame(self.influx.get_last_minutes('HM310P','Set OCP'), columns=['Time', 'Set OCP'])
    df_P = pd.DataFrame(self.influx.get_last_minutes('HM310P','Power'), columns=['Time', 'Power'])
    df_U = pd.DataFrame(self.influx.get_last_minutes('HM310P','Voltage'), columns=['Time', 'Voltage'])
    df_I = pd.DataFrame(self.influx.get_last_minutes('HM310P','Current'), columns=['Time', 'Current'])
    df_out = pd.DataFrame(self.influx.get_last_minutes('HM310P','Output'), columns=['Time', 'Output'])
    df = reduce(lambda left,right: pd.merge(left,right, on='Time', how='inner'), [df_sU, df_sI, df_OVP, df_OCP, df_P, df_U, df_I, df_out])
    
    df['Time'] = df['Time'].apply(lambda x: x.tz_convert('Europe/Berlin'))
    return df

  def get_graph(self):
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    df_U = pd.DataFrame(self.influx.get_last_minutes("HM310P", 'Voltage'), columns=['Time', 'Voltage'])
    fig.add_trace(go.Scatter(y=df_U['Voltage'], x=df_U['Time'], name='Voltage'), secondary_y=True)
    fig.update_yaxes(title_text='Voltage', secondary_y=True)

    df_I = pd.DataFrame(self.influx.get_last_minutes('HM310P','Current'), columns=['Time', 'Current'])
    fig.add_trace(go.Scatter(y=df_I['Current'], x=df_I['Time'], name='Current'), secondary_y=False)
    fig.update_yaxes(title_text='Current', secondary_y=False)

    fig.update_xaxes(title_text='Time')
    fig.update_layout(clickmode='event+select')
    return fig

  def get_table(self):
    df = self.get_data()[::-1]
    return df.to_dict(orient='records'), [{"name": i, "id": i} for i in df.columns]

  def get_values(self):
    return "%.2f" % self.influx.get_last('HM310P','Set Voltage'), "%.3f" % self.influx.get_last('HM310P','Set Current'), "%.2f" % self.influx.get_last('HM310P','Voltage'), "%.3f" % self.influx.get_last('HM310P','Current')

  def get_output_state(self):
    out = self.influx.get_last('HM310P','Output')
    state = (('ON','success') if out else ('OFF', 'danger'))
    return state
