import pandas as pd
import random
import time
import datetime as dt
import plotly.graph_objects as go
import dash_bootstrap_components as dbc
from dash import dcc, html, dash_table

class Sim:
  dmm_card = dbc.Card([
        html.H4("Owon XDM1041", className="card-title"),
        dbc.Row([
        dbc.Col([
          dbc.Row(dcc.Graph(id='dmm_graph')),
          dbc.Row(dash_table.DataTable(id='dmm_tbl',
                        page_size=10,
                        page_current=0,
                        persistence=True))
        ]),
        dbc.Col([
          dbc.Row([html.H1("XX.XXX", id='dmm_primary_value', style={'textAlign': 'center'}),html.H1("VDC", id='dmm_primary_unit', style={'textAlign': 'right', 'color':'gray'}),]),
          html.Br(),
          dbc.Row([html.H2("XX.XXX", id='dmm_secondary_value', style={'textAlign': 'center'}),html.H2("VDC", id='dmm_secondary_unit', style={'textAlign': 'right', 'color':'gray'}),]),
          html.Br(),
        ])
      ])
      ])

  def __init__(self):
    self.data = pd.DataFrame({'Time': [dt.datetime.now().strftime('%Y/%m/%d %H:%M:%S')],'Current': [0], 'Voltage': [0]})

  def get_layout(self):
    return self.dmm_card

  def read_data(self):
    self.data = pd.concat([pd.DataFrame({'Time': [dt.datetime.now().strftime('%Y/%m/%d %H:%M:%S')],'Current': [random.random()],'Voltage': [random.random()]}), self.data])
    time.sleep(1)
    return True

  def get_data(self):
    return self.data.reset_index()

  def get_graph(self):
    fig = go.Figure()
    df = self.get_data()
    df = df[(pd.to_datetime(df['Time']) >= (dt.datetime.now() - dt.timedelta(minutes=5)))][::-1]
    fig.add_trace(go.Scatter(y=df['Voltage'], x=df['Time'], name='Voltage'))
    fig.add_trace(go.Scatter(y=df['Current'], x=df['Time'], name='Current'))
    return fig

  def get_table(self):
    df = self.get_data()[:1000]
    df.drop('index', axis=1, inplace=True)
    return df.to_dict(orient='records'), [{"name": i, "id": i} for i in df.columns]

  def write_data(self,i):
    self.data = pd.concat([pd.DataFrame({'Time': [dt.datetime.now().strftime('%Y/%m/%d %H:%M:%S')],'Current': [i],'Voltage': [i]}), self.data])
    time.sleep(1)
    return True
