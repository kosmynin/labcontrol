import serial
from pymodbus.client import ModbusSerialClient

class SCPI:
  def __init__(self, lock, port, baud=115200, timeout=10):
    self.ser = None
    self.port = port
    self.baudrate = baud
    self.bytesize = serial.EIGHTBITS
    self.parity = serial.PARITY_NONE
    self.stopbits = serial.STOPBITS_ONE
    self.timeout = timeout
    self.lock = lock
    self.open()

  def open(self):
    try:
      self.ser = serial.Serial(self.port, self.baudrate, self.bytesize, self.parity, self.stopbits, self.timeout)
    except:
      print('Failed to open ', self.port)

  def close(self):
    self.ser.flush()
    self.ser.reset_input_buffer()
    self.ser.reset_output_buffer()
    self.ser.close()

  def _cmd(self, command):
    self.lock.acquire()
    if self.ser == None:
      raise Exception("Connection is not open!")
    self.ser.write(bytes(command, 'utf-8') + b"\n")
    ret = self.ser.readline().decode(errors='backslashreplace')
    self.lock.release()
    return ret[:-2]

  def query(self, cmd):
    Successful = False
    n = 0
    while not Successful:
      s = self._cmd(cmd)
      if s != '':
        Successful = True
        if s.find(',') != -1:
          s = s.split(',')[0]

        if s.endswith('\\xa6\\xb8'):
          s = s[:-8] +'Ω'
        elif s.endswith('\\xa6\\xccF'):
          s = s[:-9] +'µF'
        elif s.endswith('\\xa1\\xe6'):
          s = s[:-8] +'℃'
        elif s.endswith('\\xa8H'):
          s = s[:-5] +'℉'
        elif s.endswith('"'):
          s = s.strip('"')
        Res = s
      else:
        n = n + 1
        if n > 5:
          return '?'
    return Res

class modbus:
  def __init__(self, port, baud=9600, timeout=2):
    self.ser = None
    self.port = port
    self.baudrate = baud
    self.bytesize = 8
    self.parity = 'N'
    self.stopbits = 1
    self.timeout = timeout
    self.open()

  def open(self):
    try:
      self.ser = ModbusSerialClient(port=self.port, baudrate=self.baudrate, sytesize=self.bytesize, parity=self.parity, stopbits=self.stopbits, timeout=self.timeout)
      self.ser.connect()
    except:
      print('Failed to open ', self.port)

  def close(self):
    self.ser.close()

  def write(self, adr, val):
    self.ser.write_register(adr, val)

  def read(self, adr):
    read = self.ser.read_holding_registers(adr,slave=1)
    return read.registers[0]
